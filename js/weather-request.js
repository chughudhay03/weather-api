const API_KEY = "3aa9b280bb0e9439a670520c38463e3f";
const BASE_URL = "http://api.openweathermap.org/data/2.5/forecast";

const getWeather = async (city) => {
    const http = new slhttp();
    const url = `${BASE_URL}?q=${city}&units=metric&appid=${API_KEY}`;
    try {
        const res = await http.get(url);

        return {
            state: 'success',
            weatherInfo: res
        };
    } catch(e) {
          return {
            state: 'error',
            weatherInfo: null
          };  
    }
}
// console.log(getWeather("Goa"));